Android AOP Example with Javassist
===

# Components:

- `app`: Example Android project
- `aophijack`: Currently useless
- `plugin`: A gradle plugin, it's a separate project, you build it manully, produce artifact in local maven repository


# build

1. build & install `plugin` module first, see [plugin module README](./plugin/README.md) for how
2. build this project as usual
