package run.yang.aop.javaassist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import run.yang.aop.hijack.ChatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = "MainActivity";

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Button testButton = (Button) findViewById(R.id.btn_test_new_listener);
    testButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Log.d(TAG, "onClick() called with: view = [" + view + "]");
      }
    });

    findViewById(R.id.btn_test_activity).setOnClickListener(this);
    findViewById(R.id.btn_library_activity).setOnClickListener(this);
  }

  @Override public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btn_test_activity:
        Log.d(TAG, "Activity.onClick");
        break;

      case R.id.btn_library_activity:
        Log.d(TAG, "Go to library class");
        startActivity(new Intent(this, ChatActivity.class));
        break;
    }
  }
}
