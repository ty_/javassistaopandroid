package run.yang.aop.plugin

import org.gradle.api.Project

class ClickStatsExtension {

  /**
   * 需要处理的包，应当只包括自己的包名和使用的UI库的包名*/
  HashSet<String> includePackage = []

  ClickStatsExtension(Project project) {}
}
