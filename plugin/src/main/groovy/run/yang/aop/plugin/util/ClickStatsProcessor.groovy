package run.yang.aop.plugin.util

import javassist.ClassPool
import javassist.CtClass
import javassist.CtMethod
import org.apache.commons.io.IOUtils
import run.yang.aop.plugin.AndroidClassPool

import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarOutputStream
import java.util.zip.ZipEntry

class ClickStatsProcessor {

  private static final CtClass sOnClickListenerClass = AndroidClassPool.getClassPool().
      get("android.view.View\$OnClickListener")

  public
  static processJar(File jarFile, HashSet<String> includePackage) {

    boolean isUpdate = false
    if (jarFile) {
      println("processJar:" + jarFile.absolutePath)


      def optJar = new File(jarFile.getParent(), jarFile.name + ".opt")

      def file = new JarFile(jarFile);
      Enumeration enumeration = file.entries();
      JarOutputStream jarOutputStream = new JarOutputStream(new FileOutputStream(optJar));

      while (enumeration.hasMoreElements()) {
        JarEntry jarEntry = (JarEntry) enumeration.nextElement();
        String entryName = jarEntry.getName();
        ZipEntry zipEntry = new ZipEntry(entryName);
        InputStream inputStream = file.getInputStream(jarEntry)
        jarOutputStream.putNextEntry(zipEntry);
        if (shouldProcessClassInJar(entryName, includePackage)) {
          isUpdate = true;
          println("process Class:" + entryName)
          def bytes = injectOnClickListener(inputStream);
          jarOutputStream.write(bytes);
        } else {
          jarOutputStream.write(IOUtils.toByteArray(inputStream));
        }
        jarOutputStream.closeEntry();
      }
      jarOutputStream.close();
      file.close();

      if (isUpdate) {
        if (jarFile.exists()) {
          jarFile.delete()
        }
        optJar.renameTo(jarFile)
      } else {
        println("processJar keep same:" + jarFile.absolutePath)
        optJar.delete()
      }
      //return optJar;
    }
    //return null;
  }

  private
  static byte[] injectOnClickListener(InputStream inputStream) {
    final ClassPool classPool = AndroidClassPool.getClassPool()
    final CtClass clazz = classPool.makeClass(inputStream)

    println("injectOnClickListener: " + clazz.getName())

    CtClass[] interfaces = clazz.getInterfaces()
    if (interfaces != null && interfaces.contains(sOnClickListenerClass)) {
      println("selected: " + clazz)
      CtMethod onClickMethod = clazz.getDeclaredMethod("onClick")
      onClickMethod.insertAfter("new Throwable().printStackTrace();", true)
    } else {
      println("not selected: " + interfaces)
    }

    def bytes = clazz.toBytecode()
    clazz.defrost()
    return bytes
  }

  private
  static boolean shouldProcessClassInJar(String entryName, HashSet<String> includePackage) {
    if (!entryName.endsWith(".class")) {
      return false;
    }

    if (entryName.contains("/R\$") || entryName.endsWith("/R.class") ||
        entryName.endsWith("/BuildConfig.class") ||
        entryName.contains("android/support/")) {
      return false
    };

    return NuwaSetUtils.isIncluded(entryName, includePackage)
  }

  public static byte[] processClass(File file) {
    println("processClass: " + file)

    def optClass = new File(file.getParent(), file.name + ".opt")

    FileInputStream inputStream = new FileInputStream(file);
    FileOutputStream outputStream = new FileOutputStream(optClass)

    def bytes = injectOnClickListener(inputStream);
    outputStream.write(bytes)
    inputStream.close()
    outputStream.close()
    if (file.exists()) {
      file.delete()
    }
    optClass.renameTo(file)
    return bytes
  }

  public
  static void processClassPath(String dirName, File classPath, HashSet<String> includePackage) {
    File[] classfiles = classPath.listFiles()
    println("processClassPath inputPath: " + classPath);
    classfiles.each { inputFile ->
      //def path = inputFile.absolutePath
      def path = inputFile.name
      println("processClassPath inputFile: " + path);
      if (inputFile.isDirectory()) {
        processClassPath(dirName, inputFile, includePackage)
      } else if (path.endsWith(".jar")) {
        processJar(inputFile, includePackage)
      } else if (path.endsWith(".class") && !path.contains("/R\$") &&
          !path.endsWith("/R.class") &&
          !path.endsWith("/BuildConfig.class")) {

        println("processClassPath:" + path)
        processClass(inputFile)
      }
    }
  }
}
