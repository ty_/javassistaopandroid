package run.yang.aop.plugin

import com.android.build.gradle.AppExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import run.yang.aop.plugin.transform.PreDexTransform

public class ClickStatsPlugin implements Plugin<Project> {
  @Override
  void apply(Project project) {
    project.logger.error("========= succeed ============")

    def android = project.extensions.findByType(AppExtension)
    android.registerTransform(new PreDexTransform(project))
  }
}
