package run.yang.aop.plugin.util

class NuwaSetUtils {
  public static boolean isExcluded(String path, Set<String> excludePackage,
      Set<String> excludeClass, Set<String> excludeJars) {

    for (String exclude : excludeClass) {
      if (path.equals(exclude)) {
        return true;
      }
    }
    for (String exclude : excludePackage) {
      if (path.startsWith(exclude)) {
        return true;
      }
    }

    for (String exclude : excludeJars) {
      if (path.endsWith(exclude)) {
        return true;
      }
    }
    return false;
  }

  public static boolean isIncluded(String path, Set<String> includePackage) {

    if (includePackage.size() == 0) {
      return true
    }

    for (String include : includePackage) {
      if (path.contains(include)) {
        return true;
      }
    }

    return false;
  }
}
