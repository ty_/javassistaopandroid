package run.yang.aop.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import run.yang.aop.plugin.util.ClickStatsProcessor

class NuwaPlugin implements Plugin<Project> {
  HashSet<String> includePackage = ["run.yang"]

  private static final String EXTENSION_NAME = "clickStats"

  @Override
  void apply(Project project) {
    project.extensions.create(EXTENSION_NAME, ClickStatsExtension, project)

    project.afterEvaluate {
      def extension = project.extensions.findByName(EXTENSION_NAME) as ClickStatsExtension

      ////initial include classes and exclude classes
      extension.includePackage.each {
        includeName -> includePackage.add(includeName.replace(".", "/"))
      }
      println("include:" + includePackage)

      project.android.applicationVariants.each { variant ->
        println("variant:" + variant.name)
        println("variant:" + variant.getDirName())

        def dexTaskName = "transformClassesWithDexFor${variant.name.capitalize()}"
        def dexTask = project.tasks.findByName(dexTaskName)
        if (dexTask) {
          def patchProcessBeforeDex = "patchProcessBeforeDex${variant.name.capitalize()}"
          project.task(patchProcessBeforeDex) << {
            patchProcess(project, variant, dexTask);
          }
          //insert task
          def patchProcessBeforeDexTask = project.tasks[patchProcessBeforeDex]
          patchProcessBeforeDexTask.dependsOn dexTask.taskDependencies.getDependencies(dexTask)
          dexTask.dependsOn patchProcessBeforeDexTask
        } else {
          println("not found task:${dexTaskName}")
        }

        //                def dexTask = project.tasks.findByName("transformClassesWithDexFor${variant.name.capitalize()}")
        //                if (dexTask) {
        //                    project.logger.error "dex=>${variant.name.capitalize()}"
        //
        //                    dexTask.inputs.files.files.each { File file->
        //                        project.logger.error "file inputs=>${file.absolutePath}"
        //                    }
        //
        //                    dexTask.outputs.files.files.each { File file->
        //                        project.logger.error "file outputs=>${file.absolutePath}"
        //                    }
        //                }
      }
    }
  }

  void patchProcess(Project project, def variant, def dexTask) {
    println("nuwaJarBeforeDex")
    project.logger.error "####patchProcess###"
    def dirName = variant.dirName

    Set<File> inputFiles = dexTask.inputs.files.files
    //ArrayList<String> nuwaFile = new ArrayList<>();
    //Set<File> newInputFiles = new HashSet<File>();
    inputFiles.each { inputFile ->
      def path = inputFile.absolutePath
      project.logger.error "patchProcess inputFile=>${path}"

      if (inputFile.isDirectory()) {
        ClickStatsProcessor.processClassPath(dirName, inputFile, includePackage)
      } else if (path.endsWith(".jar")) {
        ClickStatsProcessor.processJar(inputFile, includePackage)
      }
    }
  }
}


