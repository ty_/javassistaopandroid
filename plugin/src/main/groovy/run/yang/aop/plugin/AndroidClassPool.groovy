package run.yang.aop.plugin

import javassist.ClassPool;

/**
 * 创建时间: 2016/11/25 17:50 <br>
 * 作者: Yang Tianmei <br>
 * 描述:*/

public class AndroidClassPool {

  private static ClassPool sClassPool;

  static {
    sClassPool = new ClassPool(ClassPool.getDefault())
    String androidJarPath = System.getenv("ANDROID_HOME") + "/platforms/android-25/android.jar"
    sClassPool.insertClassPath(androidJarPath)
  }

  public static ClassPool getClassPool() {
    return sClassPool
  }
}
