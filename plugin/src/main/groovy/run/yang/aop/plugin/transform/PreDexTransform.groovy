package run.yang.aop.plugin.transform

import com.android.build.api.transform.*
import com.android.build.gradle.internal.pipeline.TransformManager
import com.android.utils.FileUtils
import org.apache.commons.codec.digest.DigestUtils
import org.gradle.api.Project

/**
 * see <a href="http://blog.csdn.net/u010386612/article/details/51131642"> Android热补丁动态修复技术（三）——
 * 使用Javassist注入字节码，完成热补丁框架雏形（可使用) </a>*/
public class PreDexTransform extends Transform {

  Project project

  // 添加构造，为了方便从plugin中拿到project对象，待会有用
  public PreDexTransform(Project project) {
    this.project = project
  }

  // Transfrom 在Task列表中的名字
  // TransfromClassesWithPreDexForXXXX
  @Override
  String getName() {
    return "preDex"
  }

  @Override
  Set<QualifiedContent.ContentType> getInputTypes() {
    return TransformManager.CONTENT_CLASS
  }

  @Override
  Set<QualifiedContent.Scope> getScopes() {
    return TransformManager.SCOPE_FULL_PROJECT
  }

  @Override
  boolean isIncremental() {
    return false
  }

  @Override
  void transform(Context context, Collection<TransformInput> inputs,
      Collection<TransformInput> referencedInputs,
      TransformOutputProvider outputProvider, boolean isIncremental)
      throws IOException, TransformException, InterruptedException {
    // Transfrom的inputs有两种类型，一种是目录，一种是jar包，要分开遍历

    inputs.each { TransformInput input ->

      input.directoryInputs.each { DirectoryInput directoryInput ->

        //TODO 这里可以对input的文件做处理，比如代码注入！

        // 获取output目录
        def dest = outputProvider.getContentLocation(directoryInput.name,
            directoryInput.contentTypes, directoryInput.scopes, Format.DIRECTORY)

        // 将input的目录复制到output指定目录
        dest.getParentFile().mkdirs()
        FileUtils.copyDirectory(directoryInput.file, dest)
      }

      input.jarInputs.each { JarInput jarInput ->

        //TODO 这里可以对input的文件做处理，比如代码注入！

        // 重命名输出文件（同目录copyFile会冲突）
        def jarName = jarInput.name
        def md5Name = DigestUtils.md5Hex(jarInput.file.getAbsolutePath())
        if (jarName.endsWith(".jar")) {
          jarName = jarName.substring(0, jarName.length() - 4)
        }
        def dest = outputProvider.getContentLocation(jarName + md5Name, jarInput.contentTypes,
            jarInput.scopes,
            Format.JAR)

        dest.getParentFile().mkdirs()
        FileUtils.copyFile(jarInput.file, dest)
      }
    }
  }
}