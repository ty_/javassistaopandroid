Android AOP gradle plugin
===

# build & install
```
    ./gradlew install
```

which will install this plugin in local maven repository (`~/.m2/repository`)

# how to use

In project root `build.gradle` file:

```
	buildscript {
	  repositories {
	    jcenter()
	    mavenLocal()
	  }
	  dependencies {
	    classpath 'run.yang.aop.plugin:plugin:1.0.0'
	  }
	}
```
