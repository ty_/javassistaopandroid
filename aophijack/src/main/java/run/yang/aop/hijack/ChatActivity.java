package run.yang.aop.hijack;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = "ChatActivity";

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chat);

    Button testButton = (Button) findViewById(R.id.btn_test_anonymous_inner_class);
    testButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Log.d(TAG, "onClick() called with: view = [" + view + "]");
      }
    });

    findViewById(R.id.btn_test_activity).setOnClickListener(this);

    findViewById(R.id.btn_test_static_inner_class).setOnClickListener(new OnClickHandler());
  }

  @Override public void onClick(View view) {
    int i = view.getId();
    if (i == R.id.btn_test_activity) {
      Log.d(TAG, "Activity.onClick");
    }
  }

  static class OnClickHandler implements View.OnClickListener {
    @Override public void onClick(View view) {
      Log.d(TAG, "onClick in OnClickHandler");
    }
  }
}
